from fastapi import APIRouter, File, UploadFile

from core.voice_utils import compare_similarity
from core.voice_utils import extra_feature
from core.voice_utils import check
router = APIRouter(
    prefix="/recognition",
    tags=["recognition"],
)


@router.post("/get_feature")
async def get_feature(audio1: UploadFile = File(...), audio2:UploadFile = File(...)):
    try:
        print("da vao day")
        feature_1 = extra_feature(audio=audio1.file)
        feature_1 = feature_1.detach().cpu().numpy().tolist()
        feature_2 = extra_feature(audio=audio2.file)
        feature_2 = feature_2.detach().cpu().numpy().tolist()

        data = {"result_feature": result}
        return data
    except Exception as err:
        print(err)
        return {'error': 'error during get feature'}


@router.post("/compare")
def compare(audio1: UploadFile = File(...), audio2: UploadFile = File(...)):
    try:
        print(audio1.filename)
        result = compare_similarity(feat1=audio1.file, feat2=audio2.file)
        data = {'prediction': result}
        return data
    except Exception as err:
        print(err)
        return {'error': 'error during get feature'}
@router.post("/check_audio")
async def check_audio(audio:UploadFile = File(...)):
    print(audio.filename)
    result = check(audio.file)
    print(result)
    data = {'result':result}
    return data
