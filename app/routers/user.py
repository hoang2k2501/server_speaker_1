from fastapi import APIRouter,Depends, HTTPException,Header,Body
from fastapi.encoders import jsonable_encoder

from typing import List, Optional
from server.dependencies import get_token_header
from server.database import add_user, login, update_user, user_helper
from models.user import UserSchema,UserLogin,UserUpdate, ResponseModel, ErrorResponseModel

import jwt
from decouple import config

SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30

router = APIRouter(
    prefix="",
    tags=["user"],
    # dependencies=[Depends(get_token_header)],
)
# @router.get("/", tags=["user"],dependencies=[Depends(get_token_header)])
# x_token: Optional[str] = Header(None)
# async def get_user(id_user): 
#     return [{"username": "Rick"}, {"username": "Morty"}]

@router.post("/user", tags=["user"])
async def register(user: UserSchema = Body(...)):
    new_user = jsonable_encoder(user)
    result = await add_user(new_user)
    if result is False:
        return ErrorResponseModel(201, "Số điện thoại đã được đăng ký")
    return ResponseModel(result, "User added successfully.")

@router.post("/login", tags=["user"])
async def user_login(user: UserLogin = Body(...)):
    user_login = jsonable_encoder(user)   
    result = await login(user_login)

    if result is None:
        return ErrorResponseModel(202, "Số điện thoại chưa được đăng ký")

    jwt_token = jwt.encode(user_helper(result),SECRET_KEY)
    result['token_code'] = jwt_token.decode('utf-8')

    result_update = await update_user(result)
    if result_update: 
        return ResponseModel({
            "id_token" : jwt_token
        }, "Login successfully.")
    else : 
        return ResponseModel({
        }, "Login fail")
    
