from pydantic import BaseModel, Field
from typing import Optional

class UserSchema(BaseModel):
    fullname: str = Field(...)
    number_phone: str = Field(...)
    feature: list = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "fullname": "Trần Nhật Trường",
                "number_phone" : "0961012528",
                "feature" : []
            }
        }

class UserLogin(BaseModel):
    number_phone: str = Field(...)
    feature: list = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "number_phone" : "0961012528",
                "feature" : [[1,2,3,4]]
            }
        }
class UserUpdate(BaseModel):
    token_code: str = Field(...)
    class Config:
        schema_extra = {
            "example": {
                "token_code" : "",
            }
        }
def ResponseModel(data, message):
    return {
        "data": data,
        "code": 200,
        "message": message,
    }

def ErrorResponseModel(code, message):
    return {"code": code, "message": message}